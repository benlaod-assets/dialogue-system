![Preview of dialogue Editor](PreviewCapture.png "Preview of dialogue Editor")
## Dependencies

- [Game Event](https://gitlab.com/benlaod-assets/game-event)
- TextMeshPro

## How to install it ?

[Set a Scoped Registry first](https://gitlab.com/benlaod-assets/benlaod-assets/-/blob/main/README.md)

In your package manager, click on the '+' button then 'Add package by name...'.
Then past the following URL `fr.benjaminbrasseur.dialoguesystem`.

## How to use it ?

### Create a dialogue

In the Toolbar, open `Helpers > DialogueEditor`.
Create an asset `Tools > Dialogue Windows` that you can called whatever you want.

While in your DialogueEditor, select the created asset.
You should see the `Dialogue START` Node.

After editing you dialogue, you just have to click on `Save` at the top left of the Window.

You can create 3 types of Nodes.

#### Message<sup>[1](#note1)</sup>
You can define a text message (multiline), an avatar Sprite<sup>[2](#note2)</sup>, and a voice AudioClip<sup>[2](#note2)</sup>.

You can specify if you want the message to be followed by another message, or by a list of responses.

#### Event<sup>[1](#note1)</sup>

An event allow you to end the conversation by sending a GameEvent<sup>[3](#note3)</sup>

#### Response

A response is a node that contains a simple message. 
Multiple responses can be linked to a single message.

And you can link a message or an event node to a response.

### Read the dialogue<sup>[4](#note4)</sup>

To read a dialogue, you just need to add the `DialogueReader` component.

Then, you just have to define the following values in references of your component : 
- Current Dialogue : The Dialogue Window asset you want to read. <br/>
  (This value can be empty, and defined through the method `ChangeDialogue` of the component)
- Dialogue Box : The Canvas Element that contains all your dialogues
- Avatar<sup>[2](#note2)</sup>: The Image Element (Canvas) that will display the avatar Sprite.
- Voice Player<sup>[2](#note2)</sup>: The AudioPlayer that will play the voice AudioClip.
- Dialogue Text : The Text that will be renderer.
- Response Box : The Canvas Element that contains the responses. <br/>
  (I use a Vertical Layout Group, but you can do whatever you want !)
- Response Content Prefab : A Prefab that contains a `ResponseContent` Component a the root. <br/>
  (The prefab also need a TextMeshPro Text Canvas Element referencing to the Text variable of the component.)

To run the dialogue, you just need to toggle your action (A Input or Button) to the `DoNextDialogue` method. <br/>
If you want to display a specific message, you can specify the guid of the node you want to render. <br/>
But you usually just want to let it empty by default.

## Notes
- <sup id="note1">1</sup> This node can be linked to `Dialogue START`.
- <sup id="note2">2</sup> Those values are not mandatory and can be let empty.
- <sup id="note3">3</sup> Documentation to [GameEvent](https://gitlab.com/benlaod-assets/game-event)
- <sup id="note4">4</sup> The methods `ChangeDialogue` and `DoNextDialogue` can be override for your needs.