﻿using BenlaodAssets.DialogueSystem.Runtime.Scripts.ScriptableObjects.Gameplay.DialogueNodes;
using UnityEditor.Experimental.GraphView;

namespace BenlaodAssets.DialogueSystem.Editor.Scripts.Helpers.Editor.DialogueEditor
{
    public class StartNode : BaseNode<SoStartNode>
    {
        public StartNode()
        {
        }

        public StartNode(string guid): base(guid)
        {
        }

        public StartNode(SoStartNode node) : base(node)
        {
        }

        public override BaseNode CreateNode(DialogueGraphView graphView)
        {
            var sN = new StartNode()
            {
                title = "Start"
            };
            sN.capabilities &= ~Capabilities.Deletable;

            var generatedPort = GetPortInstance(sN, Direction.Output);
            generatedPort.portName = "First Message";
            sN.outputContainer.Add(generatedPort);
            
            return sN;
        }
    }
}