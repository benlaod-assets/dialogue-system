using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace BenlaodAssets.DialogueSystem.Editor.Scripts.Helpers.Editor.DialogueEditor
{
    public class DialogueGraphView : GraphView
    {
        public new class UxmlFactory : UxmlFactory<DialogueGraphView, UxmlTraits>
        {
        }

        private StyleSheet _css;

        private List<BaseNode> _nodes;
        private ContextualMenuManipulator _currentMenu;
        
        public DialogueGraphView()
        {
            _nodes = new List<BaseNode>();
            Insert(0, new GridBackground());

            InitManipulator();
            
            styleSheets.Add(Resources.Load<StyleSheet>("DialogueEditor"));
            viewport.EnableInClassList("GridBackground", true);
        }

        private void InitManipulator()
        {
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new ContentZoomer());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());            
            
            this.RemoveManipulator(_currentMenu);
            _currentMenu = new ContextualMenuManipulator(_ => { });
            this.AddManipulator(_currentMenu);
        }

        public void ClearNodes()
        {
            _nodes = new List<BaseNode>();
            RefreshMenu();
        }
        
        public void AddNode(BaseNode bN)
        {
            _nodes.Add(bN);
            RefreshMenu();
        }

        private void RefreshMenu()
        {
            this.AddManipulator(new ContextualMenuManipulator(e =>
            {
                _nodes.ForEach(n =>
                {
                    e.menu.AppendAction($"Add{n.GetNodeName()}", 
                        action => AddElement(n.CreateNode(this).SaveNode(action.eventInfo.mousePosition)));
                });
            }));
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var compatiblePorts = new List<Port>();
            ports.ForEach(p =>
            {
                var isValidDirection = (startPort.direction == Direction.Output && p.direction == Direction.Input)
                                       || (startPort.direction == Direction.Input && p.direction == Direction.Output);
                
                if (!isValidDirection)
                {
                    return;
                }
                
                var trueStartPort = startPort.direction == Direction.Output ? startPort : p;
                var trueAimedPort = startPort.direction == Direction.Input ? startPort : p;
                
                var condition = true;
                _nodes.ForEach(n =>
                {
                    if (trueAimedPort.node.GetType() != n.GetType()) return;

                    condition &= n.IsInputCompatible(trueStartPort);
                });
                if (startPort != p && startPort.node != p.node && condition) compatiblePorts.Add(p);
            });

            return compatiblePorts;
        }
    }
}