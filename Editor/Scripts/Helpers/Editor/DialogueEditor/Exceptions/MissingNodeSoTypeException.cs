﻿using System;

namespace BenlaodAssets.DialogueSystem.Editor.Scripts.Helpers.Editor.DialogueEditor.Exceptions
{
    public class MissingNodeSoTypeException : Exception
    {
        public MissingNodeSoTypeException(Type type) : base(
            $"The {type.Name} don't have a SoNode equivalent. Every BaseNode should have their SoNode equivalent.")
        {
        }
    }
}