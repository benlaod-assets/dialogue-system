using System;
using System.Text.RegularExpressions;
using BenlaodAssets.DialogueSystem.Runtime.Scripts.ScriptableObjects.Gameplay.DialogueNodes;
using JetBrains.Annotations;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace BenlaodAssets.DialogueSystem.Editor.Scripts.Helpers.Editor.DialogueEditor
{
    public class BaseNode : Node
    {
        public string Guid;

        public BaseNode()
        {
            Guid = System.Guid.NewGuid().ToString();
        }

        public BaseNode(string guid)
        {
            Guid = guid ?? System.Guid.NewGuid().ToString();
        }

        public String GetNodeName()
        {
            return Regex.Replace(GetType().Name, "([A-Z])", " $1");
        }

        /**
         * Declare the Node to the GraphView so that it can be added to your Dialogue
         */
        public void AddNodeToGraphView(DialogueGraphView graph)
        {
            graph.AddNode(CreateNode(graph));
        }

        public virtual bool IsInputCompatible(Port startPort)
        {
            return true;
        }

        /**
         * Initiate the values and Fields of the Node
         */
        public virtual BaseNode CreateNode(DialogueGraphView graphView)
        {
            return new BaseNode();
        }

        public BaseNode SaveNode(Vector2 position)
        {
            RefreshExpandedState();
            RefreshPorts();
            SetPosition(new Rect(position, NodeData.DefaultNodeSize));

            return this;
        }

        protected Port GetPortInstance(Node node, Direction nodeDirection,
            Port.Capacity capacity = Port.Capacity.Single)
        {
            return node.InstantiatePort(Orientation.Horizontal, nodeDirection, capacity, typeof(float));
        }
    }

    public class BaseNode<TSoType> : BaseNode where TSoType : SoNode, new()
    {
        public static Type GetSoType()
        {
            return typeof(TSoType);
        }

        public BaseNode()
        {
        }

        public BaseNode(string guid): base(guid)
        {
        }

        public BaseNode(TSoType node)
        {
            Guid = node.guid;
        }

        public virtual BaseNode<TSoType> RefreshNodeData(TSoType node)
        {
            Guid = node.guid;
            return this;
        }
        
        public virtual TSoType ConvertToSoNode()
        {
            return new TSoType()
            {
                guid = Guid,
                position = GetPosition().position
            };
        }
    }
}