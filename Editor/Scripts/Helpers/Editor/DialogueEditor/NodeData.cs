﻿using UnityEngine;

namespace BenlaodAssets.DialogueSystem.Editor.Scripts.Helpers.Editor.DialogueEditor
{
    public static class NodeData
    {
        public static readonly Vector2 DefaultNodeSize = new(200, 150);
    }
}