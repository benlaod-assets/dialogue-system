using System;
using System.Collections.Generic;
using System.Linq;
using BenlaodAssets.DialogueSystem.Editor.Scripts.Helpers.Editor.DialogueEditor.Exceptions;
using BenlaodAssets.DialogueSystem.Runtime.Scripts.ScriptableObjects.Gameplay;
using BenlaodAssets.DialogueSystem.Runtime.Scripts.ScriptableObjects.Gameplay.DialogueNodes;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace BenlaodAssets.DialogueSystem.Editor.Scripts.Helpers.Editor.DialogueEditor
{
    public class DialogueEditor : EditorWindow
    {
        [MenuItem("Helpers/DialogueEditor")]
        public static void OpenWindow()
        {
            var wnd = CreateInstance<DialogueEditor>();
            wnd.titleContent = new GUIContent("DialogueEditor");
            wnd.Show();
        }

        private DialogueGraphView _graph;

        private Label _toolbarLabel;
        private DialogueWindows _dialogueWindows;

        private List<Type> _knownEditorTypes;

        public void CreateGUI()
        {
            InitGUI();
        }

        private void InitGUI()
        {
            // Each editor window contains a root VisualElement object
            var root = rootVisualElement;

            root.Clear();

            _knownEditorTypes = new List<Type>();
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                if (!assembly.FullName.Contains("DialogueNodes.Editor")) continue;

                var baseType = $"{typeof(BaseNode<>).Namespace}.{typeof(BaseNode<>).Name}";
                foreach (var type in assembly.GetTypes())
                {
                    if (type.BaseType == null) continue;

                    var parentType = $"{type.BaseType.Namespace}.{type.BaseType.Name}";
                    if (baseType != parentType) continue;

                    if (type.BaseType?.GenericTypeArguments.Length != 1)
                    {
                        throw new MissingNodeSoTypeException(type);
                    }

                    _knownEditorTypes.Add(type);
                }
            }

            if (_knownEditorTypes.Count == 0)
            {
                return;
            }


            _graph = new DialogueGraphView();
            root.Add(_graph);
            _graph.StretchToParentSize();

            _graph.ClearNodes();
            _knownEditorTypes.ForEach(t =>
            {
                if (t.FullName == null) return;

                var obj = Activator.CreateInstance(t);
                obj?.GetType().GetMethod("AddNodeToGraphView")?.Invoke(obj, new object[] { _graph });
            });
            _knownEditorTypes.Add(typeof(StartNode));

            var toolbar = new Toolbar();
            _toolbarLabel = new Label("No DialogueWindows loaded");
            toolbar.Add(_toolbarLabel);
            toolbar.Add(new Button(() =>
            {
                _dialogueWindows =
                    AssetDatabase.LoadAssetAtPath<DialogueWindows>(
                        AssetDatabase.GetAssetPath(_dialogueWindows.GetInstanceID()));

                var dictionary = new Dictionary<string, SoNode>();

                var nodeList = new Stack<BaseNode>();
                nodeList.Push((BaseNode)_graph.nodes.First());
                while (nodeList.Count > 0)
                {
                    var currentNode = nodeList.Pop();

                    var linkGuids = new List<string>();

                    if(HasPortFromNode(currentNode, false))
                    {
                        var currentPort = GetPortFromNode(currentNode, false);
                        var linkedNodes = currentPort.connections.Select(e => e.input.node).ToList();
                        linkedNodes.ForEach(e =>
                        {
                            var baseNode = (BaseNode)e;
                            if (linkGuids.Contains(baseNode.Guid)) return;

                            linkGuids.Add(baseNode.Guid);
                            if (baseNode.Guid != currentNode.Guid && !dictionary.ContainsKey(baseNode.Guid))
                            {
                                nodeList.Push(baseNode);
                            }
                        });
                    } 
                    
                    var soNode = (SoNode)currentNode.GetType().GetMethod("ConvertToSoNode")
                        ?.Invoke(currentNode, Array.Empty<object>());

                    if (soNode == null) continue;

                    soNode.nextNodeGuid = linkGuids;
                    dictionary[soNode.guid] = soNode;

                }

                _dialogueWindows.dictionary.Clear();
                foreach (var (key, node) in dictionary)
                {
                    _knownEditorTypes.ForEach(t =>
                    {
                        if (node.GetType().FullName != t.BaseType?.GenericTypeArguments[0].FullName) return;

                        _dialogueWindows.dictionary.Set(key, node);
                    });
                }

                EditorUtility.SetDirty(_dialogueWindows);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }) { text = "Save" });
            root.Add(toolbar);

            // A stylesheet can be added to a VisualElement.
            // The style will be applied to the VisualElement and all of its children.
            root.styleSheets.Add(Resources.Load<StyleSheet>("DialogueEditor"));
        }

        private void OnSelectionChange()
        {
            var window = Selection.activeObject as DialogueWindows;
            if (window)
            {
                InitGUI();
                _dialogueWindows = window;
                _dialogueWindows =
                    (DialogueWindows)AssetDatabase.LoadMainAssetAtPath(
                        AssetDatabase.GetAssetPath(_dialogueWindows.GetInstanceID()));

                if (_dialogueWindows == null) return;

                _toolbarLabel.text = window.name;

                var nodes = new List<BaseNode>();

                var edgeDic = new Dictionary<string, List<string>>();

                if (_dialogueWindows.dictionary == null ||
                    _dialogueWindows.dictionary.Size() == 0)
                {
                    var sN = new StartNode().CreateNode(_graph).SaveNode(new Vector2(100, 200));
                    _graph.AddElement(sN);
                    return;
                }

                _dialogueWindows.dictionary.ToList().ForEach(nodePair =>
                {
                    var node = nodePair.Value;

                    _knownEditorTypes.ForEach(t =>
                    {
                        if (node.GetType().FullName != t.BaseType?.GenericTypeArguments[0].FullName) return;

                        edgeDic.Add(node.guid, node.nextNodeGuid);
                        var bNode = Activator.CreateInstance(t, node);
                        var createdNode = (BaseNode)t.GetMethod("CreateNode")
                            ?.Invoke(bNode, new object[] { _graph });
                        createdNode = (BaseNode)t.GetMethod("RefreshNodeData")?
                            .Invoke(createdNode, new object[] {node});

                        if (createdNode == null) return;

                        createdNode.SaveNode(node.position);

                        nodes.Add(createdNode);
                    });
                });

                nodes.ForEach(n => { _graph.AddElement(n); });

                _graph.nodes.ForEach(nd =>
                {
                    var n = (BaseNode)nd;
                    edgeDic[n.Guid].ForEach(e1 =>
                    {
                        if (_graph.edges.Count(e =>
                                ((BaseNode)e.input.node).Guid == e1 && ((BaseNode)e.output.node).Guid == n.Guid) >
                            0) return;

                        var edge = new Edge()
                        {
                            output = GetPortFromNode(n, false),
                            input = GetPortFromNode(nodes.First(n2 => n2.Guid == e1), true)
                        };

                        if (edge.output != null && edge.input != null)
                        {
                            edge.input.Connect(edge);
                            edge.output.Connect(edge);
                            _graph.AddElement(edge);
                        }
                    });
                });
            }
        }

        private static Port GetPortFromNode(Node node, bool isInput)
        {
            var container = isInput ? node.inputContainer : node.outputContainer;
            var currentGroupPort = container.Children()
                .First(e => e is Port or GroupBox);
            if (currentGroupPort is GroupBox)
                currentGroupPort = currentGroupPort.Children().First(e => e is Port);

            return currentGroupPort as Port;
        }

        private static bool HasPortFromNode(Node node, bool isInput)
        {
            var container = isInput ? node.inputContainer : node.outputContainer;
            var currentGroupPort = container.Children()
                .FirstOrDefault(e => e is Port or GroupBox);
            if (currentGroupPort is GroupBox)
                currentGroupPort = currentGroupPort.Children().FirstOrDefault(e => e is Port);

            return currentGroupPort != default;
        }
    }
}