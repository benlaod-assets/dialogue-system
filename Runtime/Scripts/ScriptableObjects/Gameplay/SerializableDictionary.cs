using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BenlaodAssets.DialogueSystem.Runtime.Scripts.ScriptableObjects.Gameplay
{
    [Serializable]
    public class SerializableDictionary<TKey, TValue> : ISerializationCallbackReceiver
    {
        [SerializeReference] public List<TKey> keys = new();
        [SerializeReference] public List<TValue> values = new();

        public Dictionary<TKey, TValue> Dictionary = new();

        public bool Has(TKey key)
        {
            return Dictionary.ContainsKey(key);
        }

        public TValue Get(TKey key)
        {
            return Dictionary[key];
        }

        public void Set(TKey key, TValue value)
        {
            Dictionary[key] = value;
        }

        public int Size()
        {
            return Dictionary.Count;
        }

        public List<KeyValuePair<TKey, TValue>> ToList()
        {
            return Dictionary.ToList();
        }

        public void Clear()
        {
            Dictionary.Clear();
        }

        public void OnBeforeSerialize()
        {
            keys.Clear();
            values.Clear();

            foreach (var (key, value) in Dictionary)
            {
                keys.Add(key);
                values.Add(value);
            }
        }

        public void OnAfterDeserialize()
        {
            Dictionary = new Dictionary<TKey, TValue>();

            for (var i = 0; i != Math.Min(keys.Count, values.Count); i++)
                Dictionary.Add(keys[i], values[i]);
        }
    }
}