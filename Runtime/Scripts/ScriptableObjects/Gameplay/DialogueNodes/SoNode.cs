using System;
using System.Collections.Generic;
using UnityEngine;

namespace BenlaodAssets.DialogueSystem.Runtime.Scripts.ScriptableObjects.Gameplay.DialogueNodes
{
    [Serializable]
    public class SoNode
    {
        public string guid;
        public Vector2 position;
        public List<string> nextNodeGuid;
    }
}