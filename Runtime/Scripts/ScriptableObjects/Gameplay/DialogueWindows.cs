using BenlaodAssets.DialogueSystem.Runtime.Scripts.ScriptableObjects.Gameplay.DialogueNodes;
using UnityEngine;

namespace BenlaodAssets.DialogueSystem.Runtime.Scripts.ScriptableObjects.Gameplay
{
    [CreateAssetMenu(fileName = "DialogueWindows", menuName = "Tools/Dialogue Windows", order = 0)]
    public class DialogueWindows : ScriptableObject
    {
        public SerializableDictionary<string, SoNode> dictionary;
    }
}